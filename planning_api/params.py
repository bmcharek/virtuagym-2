# DB settings
DB_HOST = 'localhost'
DB_NAME = 'planning'
DB_USER = 'root'
DB_PASSWORD = 'mysql2014'

# email settings
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'sendgrid_username'
EMAIL_HOST_PASSWORD = 'sendgrid_password'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
