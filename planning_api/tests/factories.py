import factory
from rest_framework.test import APIRequestFactory
from datetime import datetime
from planning_api.api.models import *
# from django.contrib.auth.models import User
from django.utils.crypto import random
from django.contrib.auth import get_user_model
from pytest_factoryboy import register


class UserFactory(factory.Factory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda n: u'username{0}'.format(n))
    password = factory.Sequence(lambda n: u'password{0}'.format(n))
    first_name = factory.Sequence(lambda n: u'first_name{0}'.format(n))
    last_name = factory.Sequence(lambda n: u'last_name{0}'.format(n))
    email = factory.Sequence(lambda n: u'email@{0}'.format(n))
    is_staff = True


class MemberFactory(factory.Factory):
    class Meta:
        model = Member

    member_id = factory.Sequence(lambda n: u'name {0}'.format(n))
    user = factory.SubFactory(UserFactory)
    date_joined = datetime.datetime.now()
    gender = 'MALE'
    height = '180'
    weight = '80'
    remarks = ''
    plan = 'SESSION'


class ExerciseFactory(factory.Factory):
    class Meta:
        model = Excercise

    id = 1
    name = factory.Sequence(lambda n: u'name{0}'.format(n))
    description = factory.Sequence(lambda n: u'description{0}'.format(n))
    teacher = factory.Sequence(lambda n: u'teacher {0}'.format(n))
    intensity = 'EASY'
    level = 'EASY'
    equipment = None
    burned_calories = None
    duration = datetime.time(00, 16, 00)
    distance = None


class DayFactory(factory.Factory):
    class Meta:
        model = Day

    id = 1
    name = factory.Sequence(lambda n: u'name{0}'.format(n))
    description = factory.Sequence(lambda n: u'description{0}'.format(n))
    workouts = [factory.SubFactory(ExerciseFactory)]
    date = datetime.datetime.today()


class PlanFactory(factory.Factory):
    class Meta:
        model = Plan

    id = 1
    name = factory.Sequence(lambda n: u'name{0}'.format(n))
    created_by = factory.Sequence(lambda n: u'A. Name{0}'.format(n))
    description = factory.Sequence(lambda n: u'description{0}'.format(n))
    goal = factory.Sequence(lambda n: u'A goal {0}'.format(n))
    start_date = datetime.datetime.today()
    frequency = 1
    duration = 2
    days = [factory.SubFactory(DayFactory)]


register(UserFactory)
register(ExerciseFactory)
register(DayFactory)
register(PlanFactory)
