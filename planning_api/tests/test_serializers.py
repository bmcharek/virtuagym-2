from planning_api.api.serializers import *
from .factories import *


# Todo: parametrize


def test_exercise_serializer():
    """
    Check if serializer returns the right type
    """
    serializer = ExerciseSerializer()
    f = serializer.fields.fields['name']
    obj = Excercise()
    assert f.to_representation(str(obj)) == 'Excercise object'


def test_day_serializer():
    """
    Check if serializer returns the right type
    """
    serializer = DaySerializer()
    f = serializer.fields.fields['name']
    obj = Day()
    assert f.to_representation(str(obj)) == 'Day object'


def test_plan_serializer():
    """
    Check if serializer returns the right type
    """
    serializer = PlanSerializer()
    f = serializer.fields.fields['name']
    obj = Plan()
    assert f.to_representation(str(obj)) == 'Plan object'


def test_member_serializer():
    """
    Check if serializer returns the right type
    """
    serializer = MemberSerializer()
    f = serializer.fields.fields['gender']
    obj = Member()
    assert f.to_representation(str(obj)) == 'Member object'