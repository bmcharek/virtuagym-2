import json
import pytest
from django.core.urlresolvers import reverse
from rest_framework.test import APIRequestFactory

from rest_framework.test import APIClient

from .factories import *


def test_get_an_item():
    client = APIClient()
    user = UserFactory.create(id=1)

    client.force_authenticate(user)
    response = client.get('/exercises/')
    assert response.status_code == 200
    response = client.get('/days/')
    assert response.status_code == 200
    response = client.get('/plans/')
    assert response.status_code == 200
    response = client.get('/users/')
    assert response.status_code == 200
    response = client.get('/members/')
    assert response.status_code == 200


def test_post_an_item():
    client = APIClient()
    user = UserFactory.create(id=1,
                              username='example',
                              password='password')

    client.force_authenticate(user)
    response = client.post('/exercises/',
                           {'name': 'name',
                            'intensity': 'EASY',
                            'level': 'EASY'})
    assert response.status_code == 201
    response = client.post('/days/',
                           {'name': 'name',
                            'description': 'A description',
                            "workouts": [1],
                            "date": "2017-10-02"})
    assert response.status_code == 201
    response = client.post('/plans/',
                           {"name": "name",
                            "created_by": "Its Me",
                            "description": "A description",
                            "goal": "burn",
                            "start_date": "2017-10-02",
                            "frequency": 10,
                            "duration": 3,
                            "repeated": 2,
                            "days": [1]})
    assert response.status_code == 201
    response = client.post('/users/',
                           {'username': 'user',
                            'password': '1234'})
    assert response.status_code == 201
    response = client.post('/members/',
                           {"user": 1,
                            "member_id": 1,
                            "department": "department A",
                            "date_joined": "2017-10-02",
                            "gender": "MALE",
                            "weight": 11,
                            "height": 11,
                            "plan": "SESSION",
                            "remarks": "Remarks"})
    assert response.status_code == 201


def test_update_an_item():
    client = APIClient()
    user = UserFactory.create(id=1,
                              username='example',
                              password='password')
    member = MemberFactory.create(user=user)
    client.force_authenticate(user)
    exercise = ExerciseFactory.create()
    response = client.patch('/exercises/{}'.format(exercise.id),
                            {'name': 'name 2'})
    assert response.status_code == 301
    day = DayFactory.create(workouts=[exercise])
    response = client.patch('/days/{}'.format(day.id),
                            {'name': 'day 2'})
    assert response.status_code == 301
    response = client.patch('/users/{}'.format(user.pk),
                           {'username': 'user',
                            'password': '1234'})
    assert response.status_code == 301
    response = client.patch('/members/{}'.format(member.member_id),
                           {"user": 1,
                            "member_id": 1,
                            "department": "department A",
                            "date_joined": "2017-10-02",
                            "gender": "MALE",
                            "weight": 11,
                            "height": 11,
                            "plan": "SESSION",
                            "remarks": "Remarks"})
    assert response.status_code == 301


def test_delete_an_item():
    client = APIClient()
    user = UserFactory.create(id=1,
                              username='example',
                              password='password')

    client.force_authenticate(user)
    exercise = ExerciseFactory.create()
    member = MemberFactory.create(user=user)
    response = client.delete('/exercises/{}'.format(exercise.id))
    assert response.status_code == 301
    day = DayFactory.create(workouts=[exercise])
    response = client.delete('/days/{}'.format(day.id))
    assert response.status_code == 301
    response = client.delete('/members/{}'.format(member.member_id))
    assert response.status_code == 301
    response = client.delete('/users/{}'.format(user.pk))
    assert response.status_code == 301
