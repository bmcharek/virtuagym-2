from rest_framework import generics, permissions

from rest_framework import viewsets
from .serializers import *
from .models import *


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be created, read, updated and deleted.
    """
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated,
                          permissions.IsAdminUser]


class MemberViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows members to be created, read, updated and deleted.
    """
    queryset = Member.objects.all().order_by('-date_joined')
    serializer_class = MemberSerializer
    permission_classes = [permissions.IsAuthenticated,
                          permissions.IsAdminUser]


class ExerciseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows excercises to be  created, read, updated and deleted.
    """
    queryset = Excercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = [permissions.IsAuthenticated]


class DayViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows days to be created, read, updated and deleted.
    """
    queryset = Day.objects.all()
    serializer_class = DaySerializer
    permission_classes = [permissions.IsAuthenticated]


class PlanViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows plans to be created, read, updated and deleted.
    """
    queryset = Plan.objects.all().order_by('-start_date')
    serializer_class = PlanSerializer
    permission_classes = [permissions.IsAuthenticated]

    # def create(self, request):
    # At create retrieve from kwargs all the assigned users and notify
    # Same goes for update
