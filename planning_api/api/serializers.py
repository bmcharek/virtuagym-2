from django.contrib.auth import get_user_model
from .models import *
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('url', 'username', 'email', 'groups')


class MemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ('user',
                  'member_id',
                  'department',
                  'date_joined',
                  'gender',
                  'weight',
                  'height',
                  'plan',
                  'remarks')


class ExerciseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Excercise
        fields = ('id',
                  'name',
                  'description',
                  'teacher',
                  'intensity',
                  'level',
                  'equipment',
                  'burned_calories',
                  'duration',
                  'distance'
                  )


class DaySerializer(serializers.ModelSerializer):
    # url = serializers.HyperlinkedIdentityField(
    #     view_name='days',
    #     lookup_field='id'
    #     )

    class Meta:
        model = Day
        fields = ('name',
                  'description',
                  'workouts',
                  'date')


class PlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = Plan
        fields = ('name',
                  'created_by',
                  'description',
                  'goal',
                  'start_date',
                  'frequency',
                  'duration',
                  'repeated',
                  'days')
                  # 'assigned_to')