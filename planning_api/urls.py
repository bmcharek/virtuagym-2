"""planning_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers
from planning_api.api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'members', views.MemberViewSet, 'member-detail')
router.register(r'exercises', views.ExerciseViewSet, 'exercise-detail')
router.register(r'days', views.DayViewSet, 'days-detail')
router.register(r'plans', views.PlanViewSet, 'plans-detail')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
